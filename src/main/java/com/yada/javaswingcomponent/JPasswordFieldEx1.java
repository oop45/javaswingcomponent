/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;
import javax.swing.*;
/**
 *
 * @author ASUS
 */
public class JPasswordFieldEx1 {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Password Field Example");
        JPasswordField value = new JPasswordField();
        JLabel lbl = new JLabel("Password: ");
        lbl.setBounds(20,100,80,30);
        value.setBounds(100,100,100,30);
        frame.add(value);
        frame.add(lbl);
        frame.setSize(300,300);
        frame.setLayout(null);
        frame.setVisible(true);
    }
}
