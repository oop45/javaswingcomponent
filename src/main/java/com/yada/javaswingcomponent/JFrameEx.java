/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;
import java.awt.FlowLayout;
import javax.swing.*;
/**
 *
 * @author ASUS
 */
public class JFrameEx {
    public static void main(String[] args) {
        JFrame frm = new JFrame("JFrame Example");
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        
        JLabel lbl = new JLabel("JFrame By Example");
        
        JButton btn = new JButton();
        btn.setText("Button");
        
        panel.add(lbl);
        panel.add(btn);
        frm.add(panel);
        frm.setSize(200,300);
        frm.setLocationRelativeTo(null);
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frm.setVisible(true);
    }
}
