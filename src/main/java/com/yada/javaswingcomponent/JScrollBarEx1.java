/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;
import javax.swing.*;
/**
 *
 * @author ASUS
 */
public class JScrollBarEx1 {
    JScrollBarEx1(){
        JFrame frm = new JFrame("Scrollbar Example");
        JScrollBar s = new JScrollBar();
        s.setBounds(100,100,50,100);
        frm.add(s);
        frm.setSize(400,400);
        frm.setLayout(null);
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frm.setVisible(true);
        
    }
    public static void main(String[] args) {
        new JScrollBarEx1();
    }
}
