/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;
import javax.swing.*;  
import java.awt.*;  
import java.awt.event.*;  
/**
 *
 * @author ASUS
 */
public class JDialogEx {
    private static JDialog d;
    JDialogEx() {
        JFrame frm = new JFrame();
        d = new JDialog(frm,"Dialog Example",true);
        d.setLayout(new FlowLayout());
        
        JButton btn = new JButton("OK");
        btn.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                JDialogEx.d.setVisible(false);
            }
            
        });
        
        d.add(new JLabel ("Click button to continue."));
        d.add(btn);
        d.setSize(300,300);
        d.setVisible(true);
    }
    public static void main(String[] args) {
        new JDialogEx();
    }
}
