/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JComponent;
import javax.swing.JFrame;
/**
 *
 * @author ASUS
 */
class JComponentExample extends JComponent {
    @Override
    public void paint(Graphics g){
        g.setColor(Color.green);
        g.fillRect(30, 30, 100, 100);
    }
}
    public class JComponentEx {
        public static void main(String[] args) {
            JComponentExample com = new JComponentExample();
            
            JFrame.setDefaultLookAndFeelDecorated(true);
            JFrame frm = new JFrame("JComponent Example"); 
            frm.setSize(300,200);
            frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frm.add(com);
            frm.setVisible(true);
        }
    }
