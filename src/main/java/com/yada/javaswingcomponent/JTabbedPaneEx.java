/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;
import javax.swing.*;
/**
 *
 * @author ASUS
 */
public class JTabbedPaneEx {
    JFrame frm;
    
    JTabbedPaneEx() {
        frm = new JFrame();
        JTextArea ta = new JTextArea(200,200);
        
        JPanel p1 = new JPanel();
        p1.add(ta);
        
        JPanel p2 = new JPanel();
        JPanel p3 = new JPanel();
        
        JTabbedPane tp = new JTabbedPane();
        tp.setBounds(50,50,200,200);
        tp.add("main",p1);
        tp.add("visit",p2);
        tp.add("help",p3);
        
        frm.add(tp);
        frm.setSize(400,400);
        frm.setLayout(null);
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frm.setVisible(true);
    }
    public static void main(String[] args) {
        new JTabbedPaneEx();
    }
}
