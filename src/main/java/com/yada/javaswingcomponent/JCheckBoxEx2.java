/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;

import javax.swing.*;
import java.awt.event.*;
/**
 *
 * @author ASUS
 */
public class JCheckBoxEx2 {
    JCheckBoxEx2(){
        JFrame frm = new JFrame("CheckBox Example");
        final JLabel lbl = new JLabel();
        lbl.setHorizontalAlignment(JLabel.CENTER);
        lbl.setSize(400,100);
        JCheckBox checkBox1 = new JCheckBox("C++");
        checkBox1.setBounds(150,100,50,50);
        JCheckBox checkBox2 = new JCheckBox("Java");
        checkBox2.setBounds(150,150,50,50);
        frm.add(checkBox1);
        frm.add(checkBox2);
        frm.add(lbl);
        checkBox1.addItemListener(new ItemListener(){
            @Override
            public void itemStateChanged(ItemEvent e) {
                lbl.setText("C++ Checkbox:"+ (e.getStateChange()==1?"checked":"unchecked"));
            }
            
        });
        checkBox2.addItemListener(new ItemListener(){
            @Override
            public void itemStateChanged(ItemEvent e) {
                lbl.setText("Java Checkbox:"+ (e.getStateChange()==1?"checked":"unchecked"));
            }
            
        });
        
        frm.setSize(400,400);
        frm.setLayout(null);
        frm.setVisible(true);
        }
        public static void main(String[] args) {
        new JCheckBoxEx2();
    }
    
}
