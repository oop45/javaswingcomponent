/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;
import javax.swing.*;
import java.awt.event.*;

/**
 *
 * @author ASUS
 */
public class JPopupMenuEx1 {
    JPopupMenuEx1() {
        final JFrame frm = new JFrame("PopupMenu Example");
        
        final JPopupMenu popupMenu = new JPopupMenu("Edit");
        
        JMenuItem cut = new JMenuItem("Cut");
        JMenuItem copy = new JMenuItem("Copy");
        JMenuItem paste = new JMenuItem("Paste");
        
        popupMenu.add(cut);
        popupMenu.add(copy);
        popupMenu.add(paste);
        
        frm.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                popupMenu.show(frm, e.getX(), e.getY());
            }
        });
        frm.add(popupMenu);
        frm.setSize(300,300);
        frm.setLayout(null);
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frm.setVisible(true);
    }    
    public static void main(String[] args) {
        new JPopupMenuEx1();
    }
}
