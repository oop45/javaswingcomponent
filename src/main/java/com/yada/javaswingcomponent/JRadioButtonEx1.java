/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;
import javax.swing.*;
/**
 *
 * @author ASUS
 */
public class JRadioButtonEx1 {
    JFrame frm;
    JRadioButtonEx1() {
        frm = new JFrame();
        JRadioButton r1 = new JRadioButton("A) Male");
        JRadioButton r2 = new JRadioButton("B) Female");
        r1.setBounds(75,50,100,30);
        r2.setBounds(75,100,100,30);
        ButtonGroup btng = new ButtonGroup();
        btng.add(r1);
        btng.add(r2);
        frm.add(r1);
        frm.add(r2);
        frm.setSize(300,300);
        frm.setLayout(null);
        frm.setVisible(true);
    }
    public static void main(String[] args) {
        new JRadioButtonEx1();
    }
}
