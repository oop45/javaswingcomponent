/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;
import java.awt.*;
import javax.swing.*;
/**
 *
 * @author ASUS
 */
public class JPanelEx {
    JPanelEx() {
        JFrame frm = new JFrame("Panel Example");
        
        JPanel panel = new JPanel();
        panel.setBounds(40,80,200,200);
        panel.setBackground(Color.gray);
        
        JButton btn1 = new JButton("Button 1");
        btn1.setBounds(50,100,80,30);
        btn1.setBackground(Color.yellow);
        
        JButton btn2 = new JButton("Button 2");
        btn2.setBounds(100,100,80,30);
        btn2.setBackground(Color.green);
        
        panel.add(btn1);
        panel.add(btn2);
        
        frm.add(panel);
        frm.setSize(400,400);
        frm.setLayout(null);
        frm.setVisible(true);
    }
    public static void main(String[] args) {
        new JPanelEx();
    }
}
