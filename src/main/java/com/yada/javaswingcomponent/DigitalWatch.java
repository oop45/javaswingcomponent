/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;
import javax.swing.*;
import java.awt.*;
import java.text.*;
import java.util.*;
/**
 *
 * @author ASUS
 */
public class DigitalWatch implements Runnable {
    JFrame frm;
    Thread t = null;
    int hours = 0,minutes=0,seconds=0;
    String timeString = "";
    JButton btn;
    
    DigitalWatch() {
        frm = new JFrame();
        
        t = new Thread(this);
        t.start();
        
        btn = new JButton();
        btn.setBounds(100,100,100,50);
        
        frm.add(btn);
        frm.setSize(300,400);
        frm.setLayout(null);
        frm.setVisible(true);
    }
    public void run() {
        try {
            Calendar cal = Calendar.getInstance();
            hours = cal.get(Calendar.HOUR_OF_DAY);
            if (hours > 12) hours -= 12;
            minutes = cal.get(Calendar.MINUTE);
            seconds = cal.get(Calendar.SECOND);
            
            SimpleDateFormat formatter = new SimpleDateFormat("hh:mm:ss");
            Date date = cal.getTime();
            timeString = formatter.format(date);
            
            printTime();
            
            t.sleep(1000);
        }catch (Exception e) {
            
        }
    }
    public void printTime() {
        btn.setText(timeString);
    }
    public static void main(String[] args) {
        new DigitalWatch();
    }
}
