/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;

import javax.swing.*;
import java.awt.event.*;

/**
 *
 * @author ASUS
 */
public class JScrollBarEx2 {

    JScrollBarEx2() {
        JFrame frm = new JFrame("Scrollbar Example");
        final JLabel lbl = new JLabel();
        lbl.setHorizontalAlignment(JLabel.CENTER);
        lbl.setSize(400,100);
        final JScrollBar s = new JScrollBar();
        s.setBounds(100,100,50,100);
        frm.add(s);
        frm.add(lbl);
        frm.setSize(400,400);
        frm.setLayout(null);
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frm.setVisible(true);
        s.addAdjustmentListener(new AdjustmentListener() {
            @Override
            public void adjustmentValueChanged(AdjustmentEvent e) {
                lbl.setText(("Vertical Scrollbar value is:") + s.getValue());
            }
            
        });
    }
    public static void main(String[] args) {
        new JScrollBarEx2();
    }

}
