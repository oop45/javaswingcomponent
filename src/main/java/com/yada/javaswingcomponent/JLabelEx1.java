/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;

import javax.swing.*;
/**
 *
 * @author ASUS
 */
public class JLabelEx1 {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Label Example");
        JLabel lbl1,lbl2;
        lbl1 = new JLabel("First Label.");
        lbl1.setBounds(50,50,100,30);
        lbl2 = new JLabel("Second Label.");
        lbl2.setBounds(50,100,100,30);
        frame.add(lbl1);
        frame.add(lbl2);
        frame.setSize(300,300);
        frame.setLayout(null);
        frame.setVisible(true);
    }
}
