/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
/**
 *
 * @author ASUS
 */
public class JEditorPaneEx {
    JFrame frm = null;
    
    public static void main(String[] args) {
        (new JEditorPaneEx()).test();
    }
    
    private void test() {
        frm = new JFrame("JEditorPane Test");
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frm.setSize(400,200);
        
        JEditorPane myPane = new JEditorPane();
        myPane.setContentType("text/plain");
        myPane.setText("Sleeping is necessary for a healthy body."  
                + " But sleeping in unnecessary times may spoil our health, wealth and studies."  
                + " Doctors advise that the sleeping at improper timings may lead for obesity during the students days.");
        
        frm.setContentPane(myPane);
        frm.setVisible(true);
    }
}
