/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;
import java.awt.event.*;    
import java.awt.*;    
import javax.swing.*;    
/**
 *
 * @author ASUS
 */
public class JColorChooserEx2 extends JFrame implements ActionListener {
    
    JFrame frm;
    JButton btn;
    JTextArea ta;
    
    JColorChooserEx2 () {
        frm = new JFrame("Color Chooser Example.");
        
        btn = new JButton("Pad Color");
        btn.setBounds(200,250,100,30);
        
        ta = new JTextArea();
        ta.setBounds(10,10,300,200);
        
        btn.addActionListener(this);
        
        frm.add(btn);
        frm.add(ta);
        
        frm.setLayout(null);
        frm.setSize(400,400);
        frm.setVisible(true);
    }
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        Color c = JColorChooser.showDialog(this,"Choose",Color.CYAN);
        ta.setBackground(c);
    }
    public static void main(String[] args) {
        new JColorChooserEx2();
    }
}
