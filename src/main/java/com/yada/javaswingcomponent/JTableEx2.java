/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;
import javax.swing.*;
import javax.swing.event.*;
/**
 *
 * @author ASUS
 */
public class JTableEx2 {
    public static void main(String[] args) {
        JFrame frm = new JFrame("Table EXample");
        String data[][] = { {"101","Amit","670000"},    
                          {"102","Jai","780000"},    
                          {"101","Sachin","700000"}}; 
        String column[] = {"ID","NAME","SALARY"};
        final JTable jt = new JTable(data,column);
        jt.setCellSelectionEnabled(true);
        ListSelectionModel select = jt.getSelectionModel();
        select.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        select.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                String Data = null;
                int[] row = jt.getSelectedRows();
                int[] columns = jt.getSelectedColumns();
                for (int i = 0;i < row.length; i++) {
                    for (int j =0;j < columns.length; j++) {
                        Data = (String) jt.getValueAt(row[i], columns[j]);
                    }
                }
                System.out.println("Table element selected is: " + Data);
            }
            
        });
        JScrollPane sp = new JScrollPane(jt);
        frm.add(sp);
        frm.setSize(300,200);
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frm.setVisible(true);
    }
}
