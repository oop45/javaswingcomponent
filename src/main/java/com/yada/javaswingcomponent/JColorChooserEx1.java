/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;
import java.awt.event.*;    
import java.awt.*;    
import javax.swing.*;    
/**
 *
 * @author ASUS
 */
public class JColorChooserEx1 extends JFrame implements ActionListener {
    JButton btn;
    Container c;
    
    JColorChooserEx1() {
        c = getContentPane();
        c.setLayout(new FlowLayout());
        
        btn = new JButton("color");
        btn.addActionListener(this);
        c.add(btn);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        Color initialcolor = Color.RED;
        Color color = JColorChooser.showDialog(this, "Select a color", initialcolor);
        c.setBackground(color);
    }
    public static void main(String[] args) {
        JColorChooserEx1 ch = new JColorChooserEx1();
        ch.setSize(400,400);
        ch.setVisible(true);
        ch.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}
