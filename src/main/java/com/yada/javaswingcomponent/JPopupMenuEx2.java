/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;
import javax.swing.*;
import java.awt.event.*;
/**
 *
 * @author ASUS
 */
public class JPopupMenuEx2 {
    JPopupMenuEx2() {
        final JFrame frm = new JFrame("PopupMenu Example");
        final JLabel lbl = new JLabel();
        lbl.setHorizontalAlignment(JLabel.CENTER);
        lbl.setSize(400,100);
        
        final JPopupMenu popupMenu = new JPopupMenu("Edit");
        JMenuItem cut = new JMenuItem("Cut");
        JMenuItem copy = new JMenuItem("Copy");
        JMenuItem paste = new JMenuItem("Paste");
        
        popupMenu.add(cut);
        popupMenu.add(copy);
        popupMenu.add(paste);
        
        frm.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                popupMenu.show(frm, e.getX(), e.getY());
            }
        });
        
        cut.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                lbl.setText("cut MunuItem clicked.");
            } 
        });
        
        copy.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                lbl.setText("copy MunuItem clicked.");
            } 
        });
        
        paste.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                lbl.setText("paste MunuItem clicked.");
            } 
        });
        
        frm.add(lbl);
        frm.add(popupMenu);
        frm.setSize(400,400);
        frm.setLayout(null);
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frm.setVisible(true);
    }    
    public static void main(String[] args) {
        new JPopupMenuEx2();
    }
}
