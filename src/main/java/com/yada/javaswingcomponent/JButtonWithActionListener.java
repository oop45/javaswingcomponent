/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;

import java.awt.event.*;
import javax.swing.*;
/**
 *
 * @author ASUS
 */
public class JButtonWithActionListener {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Button Example");
        final JTextField txtf = new JTextField();
        txtf.setBounds(50,50,150,20);
        JButton btn = new JButton("Click Here");
        btn.setBounds(50,100,95,30);
        btn.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
               txtf.setText("Welcome");
            }
        });
        frame.add(btn);
        frame.add(txtf);
        frame.setSize(400,400);
        frame.setLayout(null);
        frame.setVisible(true);
    }
}
