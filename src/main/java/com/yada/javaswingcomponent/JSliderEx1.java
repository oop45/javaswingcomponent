/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;
import javax.swing.*;
/**
 *
 * @author ASUS
 */
public class JSliderEx1 extends JFrame{
    public JSliderEx1() {
        JSlider slider = new JSlider(JSlider.HORIZONTAL,0,50,25);
        JPanel panel = new JPanel();
        panel.add(slider);
        add(panel);
    }
    public static void main(String[] args) {
        JSliderEx1 frm = new JSliderEx1();
        frm.pack();
        frm.setVisible(true);
    }
}
