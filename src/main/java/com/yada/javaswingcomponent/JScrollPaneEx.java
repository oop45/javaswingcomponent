/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;
import java.awt.FlowLayout;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
/**
 *
 * @author ASUS
 */
public class JScrollPaneEx {
    private static final long serialVersionUID = 1L;
    
    private static void createAndShowGUI() {
        final JFrame frm = new JFrame("Scroll Pane Example");
        
        frm.setSize(500,500);
        frm.setVisible(true);
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        frm.getContentPane().setLayout(new FlowLayout());
        
        JTextArea txtArea = new JTextArea(20,20);
        JScrollPane scrollableTextArea = new JScrollPane(txtArea);
        
        scrollableTextArea.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollableTextArea.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        
        frm.getContentPane().add(scrollableTextArea);
    }
    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}
