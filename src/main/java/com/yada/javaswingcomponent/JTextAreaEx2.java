/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;

import javax.swing.*;
import java.awt.event.*;
/**
 *
 * @author ASUS
 */
public class JTextAreaEx2 implements ActionListener {
    JLabel lbl1,lbl2;
    JTextArea area;
    JButton btn;
    
    JTextAreaEx2() {
        JFrame frame = new JFrame();
        lbl1 = new JLabel();
        lbl1.setBounds(50,25,100,30);
        lbl2 = new JLabel();
        lbl2.setBounds(160,25,100,30);
        area = new JTextArea();
        area.setBounds(20,75,250,200);
        btn = new JButton("Count Words");
        btn.setBounds(100,300,120,30);
        btn.addActionListener(this);
        frame.add(lbl1);
        frame.add(lbl2);
        frame.add(area);
        frame.add(btn);
        frame.setSize(450,450);
        frame.setLayout(null);
        frame.setVisible(true);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        String text = area.getText();
        String words[] = text.split("\\s");
        lbl1.setText("Words: "+words.length);
        lbl2.setText("Characters: "+text.length());
    }
    public static void main(String[] args) {
        new JTextAreaEx2();
    }
}
