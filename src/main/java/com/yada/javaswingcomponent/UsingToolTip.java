/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;
import javax.swing.*;
/**
 *
 * @author ASUS
 */
public class UsingToolTip {
    public static void main(String[] args) {
        JFrame frm = new JFrame("Password Field Example");
        
        JPasswordField value = new JPasswordField();
        value.setBounds(100,100,100,30);
        value.setToolTipText("Enter your Password");
        
        JLabel lbl = new JLabel("Password:");
        lbl.setBounds(20,100,80,30);
        
        frm.add(value);
        frm.add(lbl);
        frm.setSize(300,300);
        frm.setLayout(null);
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frm.setVisible(true);
    }
}
